//
//  ViewController.swift
//  MiraclePills
//
//  Created by usama on 3/17/20.
//  Copyright © 2020 usama. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet var cityPickerBtn: UIButton!
    @IBOutlet var cityPicker: UIPickerView!
    // Cities Array
    let cities = ["Faisalabad", "Lahore", "Islamabad", "Karachi", "Rawalpindi"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        cityPicker.dataSource = self
        cityPicker.delegate = self
        
        
//        self.view.backgroundColor = UIColor.green
        
    }

    @IBAction func selectCity(_ sender: Any) {
    }
    
    // Picker Coding here...
    
    
    // ==> How many Components display on PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // ==> How many Rows in each components you wanna put in there.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities.count
    }
    
    // It is the title for PickerView
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "City \(cities[row])"
    }
    
    // Function when user select the option from PickerView
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cityPickerBtn.setTitle( "City " + cities[row], for: .normal)
    }
    
    
}

